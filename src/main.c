#include <ulfius.h>

#define PORT 8080

int callback_hello(const struct _u_request *request,
                   struct _u_response *response, void *user_data) {
  (void)user_data;
  (void)request;
  ulfius_set_string_body_response(response, 200, "message: Hello, World!\n");

  return U_CALLBACK_CONTINUE;
}

int main() {
  struct _u_instance instance;

  if (ulfius_init_instance(&instance, PORT, NULL, NULL) != U_OK) {
    fprintf(stderr, "Error initializing instance\n");
    return 1;
  }

  ulfius_add_endpoint_by_val(&instance, "GET", "/", NULL, 0, &callback_hello,
                             NULL);

  if (ulfius_start_framework(&instance) == U_OK) {
    printf("Server running on port %d\n", PORT);
    getchar(); // Wait for user input to stop the server
  } else {
    fprintf(stderr, "Error starting framework\n");
  }

  ulfius_stop_framework(&instance);
  ulfius_clean_instance(&instance);

  return 0;
}
